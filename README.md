# Consumentenbond contact app

## Local environment
This project is developed with node version 12.18.3 and npm 6.14.6

# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# Important!
Unfortunately, the npm run build command is still under construction and facing some issues with component library CSS inclusion. Please use dev mode instead for testing purposes. 

# run Cypress Tests locally
$ npm run dev
$ npm run cypress