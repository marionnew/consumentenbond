import createApp from './app.js'

import databaseService from './service.database.js'

const appConfig = {"apiKey":"AIzaSyAXMn23flUCmLtUNJe4BUtXalC0u5UuivY","authDomain":"project-cb-9cd32.firebaseapp.com","databaseURL":"https:\u002F\u002Fproject-cb-9cd32-default-rtdb.europe-west1.firebasedatabase.app","projectId":"project-cb-9cd32","storageBucket":"project-cb-9cd32.appspot.com","messagingSenderId":"653759731462","appId":"1:653759731462:web:9114f5ffdbee37ee35f087","measurementId":"${config.measurementId}"}

export default async (ctx, inject) => {
  const { firebase, session } = await createApp(appConfig, ctx)

  let servicePromises = []

  if (process.server) {
    servicePromises = [
      databaseService(session, firebase, ctx, inject),

    ]
  }

  if (process.client) {
    servicePromises = [
      databaseService(session, firebase, ctx, inject),

    ]
  }

  const [
    database
  ] = await Promise.all(servicePromises)

  const fire = {
    database: database
  }

    inject('fireModule', firebase)
    ctx.$fireModule = firebase

  inject('fire', fire)
  ctx.$fire = fire
}

function forceInject (ctx, inject, key, value) {
  inject(key, value)
  const injectKey = '$' + key
  ctx[injectKey] = value
  if (typeof window !== "undefined" && window.$nuxt) {
  // If clause makes sure it's only run when ready() is called in a component, not in a plugin.
    window.$nuxt.$options[injectKey] = value
  }
}