export default {
   head: {
    title: 'consumentenbond',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [
      {
				rel: "stylesheet",
				href:
					"https://unpkg.com/vue-components-lib@0.3.73/dist/awesome-components-lib.css"
			},
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: "stylesheet", href: "/tailwind.css" }
    ]
  },
  css: ["~layouts/global.css"],
  styleResources: {
		scss: ["@/assets/scss/main.scss"],
	},
  plugins: [
    { src: "~/plugins/Vuelidate", mode: "client" },
    { src: "~/plugins/Cypress", mode: 'client' }
  ],
  modules: [
    [ '@nuxtjs/firebase',
      {
        config: {
          apiKey: 'AIzaSyAXMn23flUCmLtUNJe4BUtXalC0u5UuivY',
          authDomain: 'project-cb-9cd32.firebaseapp.com',
          databaseURL: 'https://project-cb-9cd32-default-rtdb.europe-west1.firebasedatabase.app',
          projectId: 'project-cb-9cd32',
          storageBucket: "project-cb-9cd32.appspot.com",
          messagingSenderId: "653759731462",
          appId: "1:653759731462:web:9114f5ffdbee37ee35f087",
          measurementId: "${config.measurementId}"
        },
        services: {
          database: true
        }
      }
    ]
  ],
  components: true,
  buildModules: [
    '@nuxtjs/style-resources',
    '@nuxtjs/tailwindcss'
  ],
  build: {
    extractCSS: true
  }
}
