describe('Shows the contacts in a nice table', function () {
    it('goes to the contacts page and show contacts', () => {
        		
        cy.visit('/contacts');

        cy.intercept('GET', '**/contacts.json', { fixture: 'get-contacts' }).as('get-contacts');

        cy.wait('@get-contacts').then(() => {		
			cy.get('table tbody').find('tr').should('have.length', 14);
		});
    });

    it('should filter the contacts', () => {
        cy.visit('/contacts');

        cy.intercept('GET', '**/contacts.json', { fixture: 'get-contacts' }).as('get-contacts');

        cy.wait('@get-contacts').then(() => {		
			cy.get('#search').type('mayer').blur();
            cy.get('table tbody').find('tr').should('have.length', 1);
		});
    });
});