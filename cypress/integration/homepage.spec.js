const apiUrl = 'https://project-cb-9cd32-default-rtdb.europe-west1.firebasedatabase.app/contacts.json';

describe('Check basic content available', function () {
    it('visits the homepage and checks basic content', () => {
		cy.visit('/');
		cy.get('#logo').should('exist');
		cy.get('.hero').should('exist');
		cy.get('h1').contains('Contact App');
		cy.get('nav').children().should('have.length', 3);
    });
});

describe('Check mobile nav working correctly', function () {
	beforeEach(() => {
		cy.viewport('iphone-8');
	})

    it('opens and closes the mobile menu', () => {
        cy.visit('/');
		cy.get('svg.jm-icon-menu').click();
		cy.get('.mobile-menu').children().should('have.length', 3);
		cy.get('svg.jm-icon-close-thin').click();
		
    });

	it('should navigate to the right page with the mobile menu', () => {
		cy.get('svg.jm-icon-menu').click();
		cy.get('.mobile-menu').find('div:nth-child(2) span').click();;
		cy.url().should('include', '/contacts');
    });
});