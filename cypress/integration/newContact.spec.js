const nameValidation = () => {
	cy.get('#contact-name').focus();
	cy.get('#contact-name').blur();
	cy.get('#contact-name').parent().should('have.class', 'form-group--error');
	cy.get('#contact-name').type('Marionne').blur();
	cy.get('#contact-name').parent().should('not.have.class', 'form-group--error');
}

const phoneValidation = () => {
	cy.get('#contact-phone').focus();
	cy.get('#contact-phone').type('test').blur();
	cy.get('#contact-phone').parent().should('have.class', 'form-group--error');
	cy.get('#contact-phone').type('01234').blur();
	cy.get('#contact-phone').parent().should('have.class', 'form-group--error');
	cy.get('#contact-phone').clear().type('0612345678').blur();
	cy.get('#contact-phone').parent().should('not.have.class', 'form-group--error');
};

const emailValidation = () => {
	cy.get('#contact-email').focus();
	cy.get('#contact-email').type('test').blur();
	cy.get('#contact-email').parent().should('have.class', 'form-group--error');
	cy.get('#contact-email').type('test@test.com').blur();
	cy.get('#contact-email').parent().should('not.have.class', 'form-group--error');
};

describe('Check basic content available', function () {
    it('visits the homepage and checks basic content', () => {
		cy.visit('/');
		cy.get('#logo').should('exist');
		cy.get('.hero').should('exist');
		cy.get('h1').contains('Contact App');
		cy.get('nav').children().should('have.length', 3);
    });
});

describe('Add a basic contact to contactlist', function () {
	beforeEach(() => {
		cy.viewport(1920, 1080);
	})

    it('goes to the create new contact page', () => {
        cy.visit('/new-contact');
    });

	it('enters valid data for a basic contact', () => {
		cy.get('#contact-name').type('Marionne').blur();
    });

    it('calls the api to set the new contact', () => {
		cy.intercept('POST', '**/contacts.json', { fixture: 'contact' }).as('contact');

		cy.get('#add-contact-btn').click();

		cy.wait('@contact').then(() => {		
			cy.get('.alert.tw-bg-success').should('exist');
		});
    });
});

describe('Add a advanced contact to contactlist', function () {
	beforeEach(() => {
		cy.viewport(1920, 1080);
	})

    it('goes to the create new contact page', () => {
        cy.visit('/new-contact');
    });

    it('enters valid data for a advanced contact', () => {
		cy.get('#advanced').click({force: true});
		
		cy.get('#add-contact-btn').click();

		nameValidation();
		emailValidation();
		phoneValidation();
    });

    it('calls the api to set the new contact', () => {
		cy.intercept('POST', '**/contacts.json', { fixture: 'contact' }).as('contact');

		cy.get('#add-contact-btn').click();

		cy.wait('@contact').then(() => {		
			cy.get('.alert.tw-bg-success').should('exist');
		});
    });
});