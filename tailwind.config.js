const plugin = require("tailwindcss/plugin");

module.exports = {
	prefix: "tw-",
	theme: {
		height: {
			header: '150px'
		},
		backgroundImage: {
			hero: "url('/img/header_homepage.png')",
		},
        fontSize: {
            sm: ['12px', '20px'],
            base: ['14px', '24px'],
            lg: ['16px', '28px']
        },
		fill: {
			white: "#fff",
			red: "red",
			blue: "#05164c",
		},
		colors: {
			success: "#66c1b0",
			danger: "#f29098",
            blue: "#05164c",
            grey: "#f2f2f0",
			'grey-accent': "#a7a7a7",
            white: "#fff",
			action: "#004df5",
			"btn-action": {
				default: "#004df5",
				disabled: "#004df5",
				hover: "#05164c",
				focus: "#05164c",
				loading: "#0049e9"
			},
			"btn-cancel": {
				default: "#f2f2f0",
				disabled: "#f2f2f0",
				hover: "#f2f2f0",
				focus: "#f2f2f0",
				loading: "#f2f2f0"
			}
		},
		textColor: {
			white: "#fff",
			"btn-primary": {
				default: "#fff",
				hover: "#fff",
				focus: "#fff",
				active: "#fff"
			},
			"btn-action": {
				default: "#000",
				hover: "#fff",
				focus: "#fff",
				loading: "#fddfd4"
			}
		},
		borderRadius: {
			default: "4px",
			lg: "16px"
		}
	},
	variants: {
		extend: {
			borderWidth: ["hover"],
			backgroundColor: ["hover", "focus", "active", "before"],
		}	
	},
	plugins: [
		plugin(({ addVariant, e }) => {
			addVariant("before", ({ modifySelectors, separator }) => {
				modifySelectors(
					({ className }) =>
						`.${e(`before${separator}${className}`)}:before`
				);
			});
		})
	]
};
